﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfDrivingCarSimulator
{
    public class QLearnerPartition
    {
        // Q-learning variables
        private double _r = 0.038f; // exploration chance
        private double _alpha = 1.0f;
        private double _gamma = 0.8f;
        private double[,] _qMatrix;

        private static readonly int _width = 16; // 2^4 = 16 states (obstacles? true/false in each of the 4 partitions)
        private static readonly int _height = 4; // 4 legal actions per state
        //private static int[,] _stateMap; // helper matrix for 2D-1D state space mapping

        private Action _nextAction = Action.KeepGoing;

        private static Action[] _actions = new Action[] { Action.KeepGoing, Action.TurnLeft, Action.TurnRight, Action.Wait };
        private Random _rand;

        // Initialize all matrices to default values
        public QLearnerPartition()
        {
            _qMatrix = new double[_width, _height];

            for (int i = 0; i < _width; i++)
            {
                for (int j = 0; j < _height; j++)
                {
                    _qMatrix[i, j] = 0;
                }
            }

            _rand = new Random();
        }

        // determines the best action in given state based on greedy policy
        public Action GetActionGreedy(int stateID)
        {
            if (_rand.NextDouble() < _r)
            {
                // choose random exploration
                return _actions[_rand.Next(0, 4)];
            }
            var maxR = double.MinValue;
            var bestAction = 0;

            for (int action = 0; action < 4; action++)
            {
                if (_qMatrix[stateID, action] > maxR)
                {
                    maxR = _qMatrix[stateID, action];
                    bestAction = action;
                }
            }

            return _actions[bestAction];
        }

        /*
        public double UpdateQ(float currentAngle, float currentDist, int action, float nextAngle, float nextDist)
        {
            var reward = GetRewardForState(nextAngle, nextDist);
            return UpdateQ(currentAngle, currentDist, action, nextAngle, nextDist, reward);
        }*/

        public double UpdateQ(PartitionCountSensor.SensorResult sensorResult, Action action)
        {
            var state = MapToState(sensorResult);
            // next state is found by applying action to the current state,
            // ie. how the "pie chart" is rotated by the action
            var nextState = MapToState(sensorResult, action);
            _nextAction = GetActionGreedy(nextState);
            double reward = GetRewardForNextState(sensorResult, action);
            _qMatrix[state, (int)action] = _qMatrix[state, (int)action] + _alpha * (reward + _gamma * _qMatrix[nextState, (int)_nextAction] - _qMatrix[state, (int)action]);
            return reward;
        }

        public Action GetNextAction()
        {
            return _nextAction;
        }

        // converts binary representation from partitions to state id 0-16
        private int MapToState(PartitionCountSensor.SensorResult sensorResult)
        {
            int p0 = sensorResult.CountFront > 0 ? 1 : 0;
            int p1 = sensorResult.CountRight > 0 ? 1 : 0;
            int p2 = sensorResult.CountRear > 0 ? 1 : 0;
            int p3 = sensorResult.CountLeft > 0 ? 1 : 0;

            int stateID = Convert.ToInt32($"{p0}{p1}{p2}{p3}", 2);

            return stateID;
        }

        // converts binary representation from partitions to state id 0-16 based on given action
        // in other words, the sensor result "pie chart" is rotated based on the given action
        private int MapToState(PartitionCountSensor.SensorResult sensorResult, Action action)
        {
            int p0 = sensorResult.CountFront > 0 ? 1 : 0;
            int p1 = sensorResult.CountRight > 0 ? 1 : 0;
            int p2 = sensorResult.CountRear > 0 ? 1 : 0;
            int p3 = sensorResult.CountLeft > 0 ? 1 : 0;

            // default, keepGoing/wait
            string bin = $"{p0}{p1}{p2}{p3}";
            // apply the action, "rotate" the "pie chart":
            if (action == Action.TurnLeft)
                bin = $"{p3}{p0}{p1}{p2}";
            else if (action == Action.TurnRight)
                bin = $"{p1}{p2}{p3}{p0}";

            int stateID = Convert.ToInt32(bin, 2);
            return stateID;
        }

        // reward is based on number of obstacles in front partition
        private double GetRewardForState(PartitionCountSensor.SensorResult sensorResult)
        {
            if (sensorResult.CountFront > 0)
                return 1 / sensorResult.CountFront;
            else
                return 5;
        }

        // reward is based on number of obstacles in front partition after the given action is applied to the current state "pie chart"
        private double GetRewardForNextState(PartitionCountSensor.SensorResult sensorResult, Action action)
        {
            int frontCount = sensorResult.CountFront;

            if (action == Action.TurnLeft) // turnLeft
                frontCount = sensorResult.CountLeft;
            else if (action == Action.TurnRight) // turnRight
                frontCount = sensorResult.CountRear;

            if (frontCount > 0)
                return -frontCount;
            else
                return 5;
        }
    }
}
