﻿using System;
using System.Collections.Generic;

namespace SelfDrivingCarSimulator
{
    /**
     * Sensor counting the number of obstacles in 4 directional partitions: [rear, left, front, right]
     * State is defined based on obstacles in each partition.
     * Reward is based on the number of obstacles in front partition
     * No obstacles -> max reward
     */
    public class PartitionCountSensor : SensorBase
    {

        public PartitionCountSensor(WorldPanel worldPanel, World world) : base (worldPanel, world)
        {
        }

        public SensorResult Sense(Car car, SDCPoint currentPos)
        {
            int rearC = 0;
            int leftC = 0;
            int frontC = 0;
            int rightC = 0;

            foreach (var o in _world.Obstacles)
            {
                if (!o.Equals(car)) // ignore self
                {
                    var distVec = o - currentPos;
                    var vVec = new SDCVector(car.Dx, car.Dy);
                    var dist = (float)distVec.Length;
                    if (dist <= GetRange())
                    {
                        var angle = (float)(vVec.GetAngle(distVec) * 180d / Math.PI);
                        var angleLocal = car.Rot - angle;

                        if (angleLocal < 45)
                            rearC++;
                        else if (angleLocal < 135)
                            leftC++;
                        else if (angleLocal < 225)
                            frontC++;
                        else if (angleLocal < 315)
                            rightC++;
                        else
                            rearC++;

                        // check collision
                        if (dist < 5)
                        {
                            _worldPanel.HandleCollision();
                            car.HandleCollision();
                        }
                    }
                }
            }

            return new SensorResult() { CountRear = rearC, CountLeft = leftC, CountFront = frontC, CountRight = rightC };
        }

        public struct SensorResult
        {
            // obstacle count is partitioned in sectors/intervals:
            // 0-44: rear, 45-134: left, 135-224: front, 225-314: right, 315-360: rear

            public int CountRear { get; set; }
            public int CountLeft { get; set; }
            public int CountFront { get; set; }
            public int CountRight { get; set; }
        }
    }
}
