﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SelfDrivingCarSimulator
{
    /**
     * Sensor measuring relative angle and distance to closest obstacle
     * State is based on current (discretized) angle and distance to obstacle => agent wants to maximize distance
     */
    public class AngleDistSensor : SensorBase
    {
        public AngleDistSensor(WorldPanel worldPanel, World world) : base (worldPanel, world)
        {
        }

        public SensorResult Sense(Car car, SDCPoint currentPos)
        {
            
            List<SensorResult> pointsInRange = new List<SensorResult>();
            
            foreach (var o in _world.Obstacles)
            {
                if (!o.Equals(car)) // ignore self
                {
                    var distVec = o - currentPos;
                    var vVec = new SDCVector(car.Dx, car.Dy);
                    var dist = (float)distVec.Length;
                    if (dist <= GetRange())
                    {
                        var angle = (float)(vVec.GetAngle(distVec) * 180d / Math.PI);
                        var angleLocal = car.Rot - angle;
                        pointsInRange.Add(new SensorResult(dist, Math.Abs(angleLocal)));

                        if (float.IsNaN(angle))
                        {
                            Debug.WriteLine("Something has gonne terribly wrong");
                        }

                        // check collision
                        if (dist < 5)
                        {
                            _worldPanel.HandleCollision();
                            car.HandleCollision();

                        }
                    }
                }
            }

            pointsInRange.Sort();
            var res = pointsInRange.FirstOrDefault();

            return res;
        }

        public struct SensorResult : IComparable<SensorResult>
        {
            public float Distance;
            public float Angle;

            public SensorResult(float dist, float angle)
            {
                Distance = dist;
                Angle = angle;
            }

            public int CompareTo(SensorResult other)
            {
                return Distance.CompareTo(other.Distance);
            }
        }
    }
}
