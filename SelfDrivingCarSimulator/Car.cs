﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfDrivingCarSimulator
{
    public class Car : WorldObject
    {
        private float _xPos;
        private float _yPos;

        // track the total amount of travel for each agent to account for wait-action.
        // performance is then measured as collisions per distance
        private double _distanceTravelled = 0;
        private int _noOfCollisions = 0;

        // keep track of initial spawn position
        private float _spawnX;
        private float _spawnY;

        public float Dx { get; protected set; } = 0;
        public float Dy { get; protected set; } = 0;

        public SDCVector Vel;

        public float Rot { get; set; } = 0;

        public float V { get; set; } = 1;

        public string CurrentAction = "None";
        public double CurrentReward = 0;

        public Queue<SDCPoint> Trail = new Queue<SDCPoint>();
        private int tI = 0;

        protected AngleDistSensor _angleDistSensor;
        //protected PartitionCountSensor _partitionSensor;

        int _waitTimer = 0;

        private readonly QLearnerAngleDistance _qAngleDistance;
        //private readonly QLearnerPartition _qPartition;

        private Color _color;
        public Car(AngleDistSensor sensor)
        {
            _angleDistSensor = sensor;
            _color = Color.FromArgb(World.RAND.Next(256), World.RAND.Next(256), World.RAND.Next(256));
            
            PosX(World.RAND.Next(500) + 250);
            PosY(World.RAND.Next(300) + 150);

            _spawnX = _xPos;
            _spawnY = _yPos;

            _qAngleDistance = new QLearnerAngleDistance();
            //_qPartition = new QLearnerPartition();
        }

        public void LocalSimulate()
        {
            if (_waitTimer > 0)
                _waitTimer--;
            else
            {
                _xPos += Dx;
                _yPos += Dy;
                _distanceTravelled += V;
            }

            // reset car if out of bounds
            if (PosX() > _angleDistSensor.WorldWidth + 5 || PosX() < -5 || PosY() > _angleDistSensor.WorldHeight + 5 || PosY() < -5)
            {
                PosX(_spawnX);
                PosY(_spawnY);

                Dx = 0;
                Dy = 0;
            }
        }

        public void UpdateTrail()
        {
            if (tI >= 20)
            {
                if (Trail.Count > 50)
                {
                    Trail.Dequeue();
                }

                Trail.Enqueue(new SDCPoint((int)_xPos, (int)_yPos));
                tI = 0;
            }
            else
                tI++;
        }

        public virtual void Learn()
        {
            var v = new SDCVector(0, V);
            Dx = v.X;
            Dy = v.Y;
            var sensorResult = _angleDistSensor.Sense(this, new SDCPoint(PosX(), PosY()));

            Action action = _qAngleDistance.GetNextAction();
            //Action action = _qPartition.GetNextAction();
            CurrentAction = action.ToString();

            switch (action)
            {
                case Action.TurnLeft:
                    RotateV(-10);
                    break;
                case Action.TurnRight:
                    RotateV(10);
                    break;
                case Action.Wait:
                    _waitTimer = 10;
                    break;
                default:
                    break;
            }

            v.Rotate(Rot);

            Dx = v.X;
            Dy = v.Y;

            var nextPos = new SDCPoint((PosX() + Dx), (PosY() + Dy));
            var nextSensorResult = _angleDistSensor.Sense(this, nextPos);
            //double r = _qPartition.UpdateQ(sensorResult, action);
            double r = _qAngleDistance.UpdateQ(nextSensorResult.Angle, nextSensorResult.Distance, (int)action, nextSensorResult.Angle, nextSensorResult.Distance);

            CurrentReward = Math.Round(r, 2);
        }

        // rotates the direction vector. handles wrap-around
        private void RotateV(int d)
        {
            if (Rot + d >= 360)
                Rot = 360 - (Rot + d);
            else if (Rot + d < 0)
                Rot = 360 + d;
            else
                Rot += d;
        }

        public string GetCollisionLog()
        {
            return $"{_distanceTravelled};{_noOfCollisions}";
        }

        public void HandleCollision()
        {
            _noOfCollisions++;
        }

        public override void Render(Graphics g)
        {
            UpdateTrail();
            double a = 0;
            foreach (var t in Trail)
            {
                g.FillEllipse(new SolidBrush(Color.FromArgb((int)((a++ / (float)Trail.Count) * 255f), _color)), t.PosX() - 2, t.PosY() - 2, 4, 4);
            }

            g.FillRectangle(Brushes.Black, PosX() - 5.5f, PosY() - 5.5f, 10.5f, 10.5f); // black outline

            g.ResetTransform();
            g.FillRectangle(new SolidBrush(_color), PosX() - 5, PosY() - 5, 10, 10);
            g.DrawLine(Pens.Blue, PosX(), PosY(), PosX() + Dx * 100, PosY() + Dy * 100); // sensor line

            // action/reward label
            if (WorldPanel.ShowLabels)
            {
                g.FillRectangle(Brushes.Black, PosX() + 10, PosY() + 10, 180, 25);
                g.DrawString(CurrentAction + " | " + CurrentReward, new Font("Arial", 12), Brushes.White, PosX() + 15, PosY() + 15);
                //g.DrawString("Current rotation: " + Rot , new Font("Arial", 12), Brushes.White, PosX() + 15, PosY() + 15);
            }
        }
        public override float PosX()
        {
            return _xPos;
        }

        public override float PosY()
        {
            return _yPos;
        }

        public void PosX(float x)
        {
            _xPos = x;
        }

        public void PosY(float y)
        {
            _yPos = y;
        }
    }
}
