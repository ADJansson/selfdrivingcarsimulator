﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfDrivingCarSimulator
{
    public class QLearnerAngleDistance
    {
        // Q-learning variables
        private double _r = 0.038f; // exploration chance
        private double _alpha = 1.0f;
        private double _gamma = 0.8f;
        private double[,] _qMatrix;
        private static readonly int _h = 25;
        private static readonly int _w = 20; // state space resolution
        private static readonly int _width = _h * _w; // 25x20 discrete state space size
        private static readonly int _height = 4; // 3 legal actions per state
        private static int[,] _stateMap; // helper matrix for 2D-1D state space mapping

        private Action _nextAction = Action.KeepGoing;

        private static Action[] _actions = new Action[] { Action.KeepGoing, Action.TurnLeft, Action.TurnRight, Action.Wait };
        private Random _rand;

        // Initialize all matrices to default values
        public QLearnerAngleDistance()
        {
            _qMatrix = new double[_width, _height];

            for (int i = 0; i < _width; i++)
            {
                for (int j = 0; j < _height; j++)
                {
                    _qMatrix[i, j] = 0;
                }
            }

            _stateMap = new int[_h, _w];
            int z = 0;
            for (int i = 0; i < _h; i++)
            {
                for (int j = 0; j < _w; j++)
                {
                    _stateMap[i, j] = z++;
                }
            }

            _rand = new Random();
        }

        // determines the best action in given state based on greedy policy
        public Action GetActionGreedy(float angle, float dist)
        {
            if (_rand.NextDouble() < _r)
            {
                // choose random exploration
                return _actions[_rand.Next(0, 4)];
            }
            var currentState = MapToState(angle, dist);
            var maxR = double.MinValue;
            var bestAction = 0;

            for (int action = 0; action < 4; action++)
            {
                if (_qMatrix[currentState, action] > maxR)
                {
                    maxR = _qMatrix[currentState, action];
                    bestAction = action;
                }
            }

            return _actions[bestAction];
        }

        public double UpdateQ(float currentAngle, float currentDist, int action, float nextAngle, float nextDist)
        {
            var reward = GetRewardForState(nextAngle, nextDist);
            return UpdateQ(currentAngle, currentDist, action, nextAngle, nextDist, reward);
        }

        public double UpdateQ(float currentAngle, float currentDist, int action, float nextAngle, float nextDist, double reward)
        {
            var state = MapToState(currentAngle, currentDist);
            var nextState = MapToState(nextAngle, nextDist);
            _nextAction = GetActionGreedy(nextAngle, nextDist);
            _qMatrix[state, action] = _qMatrix[state, action] + _alpha * (reward + _gamma * _qMatrix[nextState, (int)_nextAction] - _qMatrix[state, action]);
            return reward;
        }

        public Action GetNextAction()
        {
            return _nextAction;
        }

        private int MapToState(float angle, float dist)
        {
            var angleD = (int)Math.Round((angle) / 360 * (_h - 1)); // 0 - 24
            var distD = (int)Math.Round(dist / 220 * (_w - 1)); // 0 - 19
            return _stateMap[angleD, distD];
        }

        // reward is based on distance from obstacle in all states
        private double GetRewardForState(float angle, float dist)
        {
            if (dist < 5)
                return -10;
            //else if (dist < 20)
            //    return 10;
            else
                return dist;
        }
    }
}
