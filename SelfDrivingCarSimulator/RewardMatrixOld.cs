﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfDrivingCarSimulator
{
    public class RewardMatrixOld
    {
        private static int _h = 25;
        private static int _w = 20;
        private static float[,] _r_mat = new float[_h, _w];
        private static Action[,] _q_mat = new Action[_h, _w];

        public enum Action
        {
            TurnLeft, // 0
            KeepGoing, // 1
            TurnRight, // 2
            Unknown
        }

        private static Action[] _actions = new Action[] { Action.TurnLeft, Action.TurnRight, Action.KeepGoing };
        private static Random RAND = new Random();

        public static void Init()
        {
            for (int i = 0; i < _h; i++)
            {
                for (int j = 0; j < _w; j++)
                {
                    _r_mat[i, j] = 0;
                    _q_mat[i, j] = Action.Unknown;
                }
            }
        }

        public static void PrintLearningProgress()
        {
            var totalCells = _w * _h;
            int k = 0;

            for (int i = 0; i < _h; i++)
            {
                for (int j = 0; j < _w; j++)
                {
                    if (_q_mat[i, j] != Action.Unknown)
                        k++;
                }
            }

            Debug.WriteLine("Progress: " + ((float)k / (float)totalCells * 100) + "%");
        }

        public static void AddReward(float angle, float dist, float reward)
        {
            // actions: turn left (-10), keep going, turn right(+10)

            var h = CalcH(angle);
            var w = CalcW(dist);

            _r_mat[h, w] = reward;

            /*
             * angle
            360[[][][][]]
            ....
            0
            ....
            -360

            0               100 <- dist
                */
        }

        private static int CalcH(float angle)
        {
            var it = (int)Math.Round(((angle + 180) / 360) * (_h - 1));
            //Debug.WriteLine(angle + " -> H: " + it);
            return it;
        }

        private static int CalcW(float dist)
        {
            var it = (int)Math.Round((dist / 120) * (_w - 1));
            //Debug.WriteLine(dist + " -> W: " + it);
            return it;
        }

        /// <summary>
        /// Get the reward for the state associated with the proposed movement
        /// </summary>
        /// <param name="newAngle"></param>
        /// <param name="newDist"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static void SetRewardForAction(float newAngle, float newDist, Action action, float reward)
        {
            var h = CalcH(newAngle);
            var w = CalcW(newDist);

            //if (_q_mat[h, w] == Action.Unknown)
            {
                _q_mat[h, w] = action;
                _r_mat[h, w] = reward;
            }
        }

        public static float GetRewardForState(float angle, float dist)
        {
            var h = CalcH(angle);
            var w = CalcW(dist);

            return _r_mat[h, w];
        }

        public static Action GetActionForState(float angle, float dist)
        {
            var h = CalcH(angle);
            var w = CalcW(dist);
            var action = _q_mat[h, w];
            
            if (action != Action.Unknown)
            {
                // 5% of testing new action
                if (RAND.Next(100) < 5)
                {
                    Debug.WriteLine("RANDOM ACTION!");
                    return _actions[RAND.Next(_actions.Length)];
                }
                else
                {
                    return action;
                }
            }
            else
            {
                // return random action if unknown
                //Debug.WriteLine("UNKNOWN ACTION!");
                return _actions[RAND.Next(_actions.Length)];
            }
        }
    }
}
