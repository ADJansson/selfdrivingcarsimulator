﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfDrivingCarSimulator
{
    public class World
    {
        private List<WorldObject> _obstacles;
        private List<Car> _cars;
        //private DumbCar _dumb_car;

        public List<WorldObject> Obstacles { get { return _obstacles; } }
        public List<Car> Cars { get { return _cars; } }
        //public DumbCar MrBean { get { return _dumb_car; } }

        public static Random RAND = new Random();

        public World(WorldPanel parent, int noOfCars)
        {
            _cars = new List<Car>();
            _obstacles = new List<WorldObject>();
            for (int i = 0; i < noOfCars; i++)
            {
                var car = new Car(new AngleDistSensor(parent, this));
                //var car = new Car(new PartitionCountSensor(parent, this));
                _cars.Add(car);
                _obstacles.Add(car);
            }

            //_dumb_car = new DumbCar(new Sensor(parent, this));
            //_obstacles.Add(_dumb_car);
        }

        public void AddObstacle(int x, int y)
        {
            _obstacles.Add(new Obstacle(x, y));
        }

        public void AddObstacle(WorldObject point)
        {
            _obstacles.Add(new Obstacle(point));
        }

        public void RemoveObstacle(int x, int y)
        {
            _obstacles.Remove(_obstacles.Where(o => o.PosX() == x && o.PosY() == y).First());
        }

        public void RemoveObstacle(SDCPoint point)
        {
            _obstacles.Remove(_obstacles.FirstOrDefault(p => (point - p).Length < 5));
        }

        public void ClearObstacle()
        {
            _obstacles.Clear();
        }
    }

    public abstract class WorldObject
    {
        public abstract float PosX();
        public abstract float PosY();

        public override bool Equals(object obj)
        {
            return Equals(obj as WorldObject);
        }

        public bool Equals(WorldObject other)
        {
            return other != null && PosX() == other.PosX() && PosY() == other.PosY();
        }

        public abstract void Render(Graphics g);
    }
}
