﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfDrivingCarSimulator
{
    public class Obstacle : WorldObject
    {
        private float _xPos;
        private float _yPos;

        public Obstacle(int x, int y)
        {
            _xPos = x;
            _yPos = y;
        }
        public Obstacle(WorldObject point)
        {
            _xPos = point.PosX();
            _yPos = point.PosY();
        }

        public override float PosX()
        {
            return _xPos;
        }

        public override float PosY()
        {
            return _yPos;
        }

        public override void Render(Graphics g)
        {
            //g.DrawImage(WorldPanel.OBSTACLE_GFX, PosX(), PosY(), 20, 20);
            g.FillEllipse(Brushes.Black, PosX(), PosY(), 10, 10);
        }
    }
}
