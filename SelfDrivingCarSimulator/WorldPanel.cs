﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Diagnostics;

namespace SelfDrivingCarSimulator
{
    public partial class WorldPanel : UserControl
    {
        private World _world;
        private int _obstacleCount = 200;

        // target position as selected by user
        private static SDCPoint _targetPos = SDCPoint.Empty;
        private bool _shiftKeyPressed = false;
        private bool _ctrlKeyPressed = false;
        private Queue<SDCPoint> _addObstacleQueue = new Queue<SDCPoint>();
        private Queue<SDCPoint> _removeObstacleQueue = new Queue<SDCPoint>();

        private static bool _showLabels = true;

        private Random _rand = new Random();

        private int _learningTimer = 0;

        public static Bitmap AGENT_GFX = new Bitmap(@"gfx\robot.png");
        public static Bitmap MASK_LAYER = new Bitmap(@"gfx\mask.png");
        public static Bitmap OBSTACLE_GFX = new Bitmap(@"gfx\obstacle.png");
        public static Bitmap SAND_GFX = new Bitmap(@"gfx\sand.jpg");

        private DateTime _startTime; // keep track of starting time to calculate collisions/min
        private long _totalCollisions = 0;

        public static SDCPoint TargetPos { get { return _targetPos; } }
        public static bool ShowLabels { get { return _showLabels; } }
        public WorldPanel()
        {
            InitializeComponent();

            SetStyle(ControlStyles.DoubleBuffer | ControlStyles.UserPaint |
            ControlStyles.AllPaintingInWmPaint, true);
            UpdateStyles();

            _world = new World(this, 20);

            for (int i = 0; i < _obstacleCount; i++)
            {
                _world.AddObstacle(_rand.Next(50, 1150), _rand.Next(50, 750));
            }

            // define world border
            for (int x = 10; x < 1150; x += 10)
            {
                _world.AddObstacle(x, 0);
                _world.AddObstacle(x, 750);
            }

            for (int y = 10; y < 750; y += 10)
            {
                _world.AddObstacle(0, y);
                _world.AddObstacle(1150, y);
            }

            _startTime = DateTime.Now;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;

            //e.Graphics.DrawImage(SAND_GFX, 0, 0, Width, Height);

            if (_targetPos != SDCPoint.Empty)
            {
                e.Graphics.FillEllipse(Brushes.Black, _targetPos.PosX() - 14, _targetPos.PosY() - 14, 28, 28);
                e.Graphics.FillEllipse(Brushes.Yellow, _targetPos.PosX() - 13f, _targetPos.PosY() - 13f, 26, 26);
            }


            for (int i = 0; i < _world.Obstacles.Count; i++)
            {
                _world.Obstacles[i].Render(e.Graphics);
            }

            foreach (var car in _world.Cars)
            {
                car.Render(e.Graphics);
            }
            
            base.OnPaint(e);
        }

        public void LocalSimulate(double dt)
        {
            // add/remove obstacles from queue
            if (_addObstacleQueue.Count > 0)
            {
                var oP = _addObstacleQueue.Dequeue();
                _world.AddObstacle(oP);
            }
            if (_removeObstacleQueue.Count > 0)
            {
                var oP = _removeObstacleQueue.Dequeue();
                _world.RemoveObstacle(oP);
            }
            foreach (var car in _world.Cars)
            {
                car.Learn();
                car.LocalSimulate();
            }
        }

        private void WorldPanel_MouseUp(object sender, MouseEventArgs e)
        {
            SDCPoint np = new SDCPoint(e.X, e.Y);

            if (e.Button == MouseButtons.Left)
            {
                if (_shiftKeyPressed)
                    _targetPos = np;
                else
                    _addObstacleQueue.Enqueue(new SDCPoint(e.X, e.Y));
            }
            else if (e.Button == MouseButtons.Right)
            {
                _removeObstacleQueue.Enqueue(new SDCPoint(e.X, e.Y));
            }
        }

        public void HandleCollision()
        {
            _totalCollisions++;
        }

        public void WriteLogs()
        {
            var dateID = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss");
            System.IO.File.WriteAllText($"log_{dateID}.csv", "ID;Distance;Collisions" + Environment.NewLine);
            for (int i = 0; i < _world.Cars.Count; i++)
            {
               System.IO.File.AppendAllText($"log_{dateID}.csv", i + ";" + _world.Cars[i].GetCollisionLog() + Environment.NewLine);
            }
        }
        private void WorldPanel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ShiftKey)
                _shiftKeyPressed = true;
            else if (e.KeyCode == Keys.Control)
                _ctrlKeyPressed = true;
        }

        private void WorldPanel_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ShiftKey)
                _shiftKeyPressed = false;
            else if (e.KeyCode == Keys.Control)
                _ctrlKeyPressed = false;
            else if (e.KeyCode == Keys.L)
                _showLabels = !_showLabels; // toggle label visibility
        }
    }
}
