﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfDrivingCarSimulator
{
    public class SDCPoint : WorldObject
    {
        public float X { get; protected set; }
        public float Y { get; protected set; }

        public static readonly SDCPoint Empty = new SDCPoint(-1, -1);

        public SDCPoint(float x, float y)
        {
            X = x;
            Y = y;
        }

        public static SDCVector operator -(SDCPoint p1, SDCPoint p2)
        {
            return new SDCVector(p2.X - p1.X, p2.Y - p1.Y);
        }

        public static SDCVector operator -(WorldObject p1, SDCPoint p2)
        {
            return new SDCVector(p2.X - p1.PosX(), p2.Y - p1.PosY());
        }

        public static SDCVector operator -(SDCPoint p1, WorldObject p2)
        {
            return new SDCVector(p2.PosX() - p1.X, p2.PosY() - p1.Y);
        }

        public double GetAngle(SDCPoint otherPoint)
        {
            return Math.Atan2(Y - otherPoint.Y, X - otherPoint.X);
        }

        public double GetAngle(WorldObject otherPoint)
        {
            return Math.Atan2(Y - otherPoint.PosY(), X - otherPoint.PosX());
        }

        public override float PosX()
        {
            return X;
        }

        public override float PosY()
        {
            return Y;
        }

        public override void Render(Graphics g)
        {
        }
    }

    public class SDCVector : SDCPoint
    {
        private double _length;

        public double Length { get { return _length; } }

        public SDCVector(float x, float y) : base(x, y)
        {
            _length = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
        }

        // heavy math shit yo
        public void Rotate(float angle)
        {
            var rMat = new MatrixR(angle);

            var x = (float)(rMat.M[0, 0] * X + rMat.M[0, 1] * Y);
            var y = (float)(rMat.M[1, 0] * X + rMat.M[1, 1] * Y);

            X = x;
            Y = y;
        }

        public float GetAngle(SDCVector b)
        {
            return (float)Math.Acos((X * b.X + Y * b.Y) / (Length * b.Length));
        }
    }

    // rotation matrix
    public class MatrixR
    {
        public double[,] M = new double[2, 2];

        public MatrixR(float angle)
        {
            angle = angle * (float)Math.PI / 180f;

            M[0, 0] = Math.Cos(angle);
            M[0, 1] = -Math.Sin(angle);
            M[1, 0] = Math.Sin(angle);
            M[1, 1] = Math.Cos(angle);
        }
    }

    public enum Action
    {
        KeepGoing, // 0
        TurnLeft, // 1
        TurnRight, // 2
        Wait // 3
    }
}
