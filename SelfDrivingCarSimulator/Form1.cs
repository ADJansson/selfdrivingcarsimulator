﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SelfDrivingCarSimulator
{
    public partial class Form1 : Form
    {
        private Thread _tR; // Render loop thread
        private Thread _tP; // Physics loop thread

        //private double _prev_elapsed = 0;
        private double _time_elapsed = 0;

        private Stopwatch _st;
        public Form1()
        {
            InitializeComponent();
            ThreadStart tsMain = new ThreadStart(RenderLoop);

            RewardMatrixOld.Init();

            // Start the stopwatch
            _st = new Stopwatch();
            _st.Start();

            _tR = new Thread(() => RenderLoop());
            _tR.Start();

            _tP = new Thread(() => PhysicsLoop());
            _tP.Start();
        }

        private void RenderLoop()
        {
            try
            {
                while (true)
                {
                    _worldPanel.Invalidate();
                    Thread.Sleep(8);
                }
            }
            catch (ThreadAbortException tae)
            {
                Debug.WriteLine(tae.Message);
            }
        }

        private void PhysicsLoop()
        {
            try
            {
                while (true)
                {
                    double elapsed = _st.Elapsed.TotalSeconds;
                    double dt = elapsed - _time_elapsed;
                    _time_elapsed = elapsed;
                    //Debug.WriteLine($"dt:{dt}");

                    _worldPanel.LocalSimulate(dt * 100);
                    Thread.Sleep(8);
                }
            }
            catch (ThreadAbortException tae)
            {
                Debug.WriteLine(tae.Message);
            }
        }

        private void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            _worldPanel.WriteLogs();

            _tR.Abort();
            _tP.Abort();

        }
    }
}
