﻿using System;
using System.Collections.Generic;

namespace SelfDrivingCarSimulator
{
    /**
     * Sensor measuring the average distance and angle of all obstacles in range
     * State is the average distance and angle of all obstacles, -> agent wants to maximize the average distance
     */
    public class AverageDistSensor : SensorBase
    {
        public AverageDistSensor(WorldPanel worldPanel, World world) : base(worldPanel, world)
        {
        }

        public SensorResult Sense(Car car, SDCPoint currentPos)
        {
            double totalDist = 0;
            int totalObstacles = 0;

            foreach (var o in _world.Obstacles)
            {
                if (!o.Equals(car)) // ignore self
                {
                    var distVec = o - currentPos;
                    var vVec = new SDCVector(car.Dx, car.Dy);
                    var dist = (float)distVec.Length;
                    if (dist <= GetRange())
                    {
                        var angle = (float)(vVec.GetAngle(distVec) * 180d / Math.PI);

                        totalDist += distVec.Length;
                        totalObstacles++;

                        // check collision
                        if (dist < 5)
                        {
                            _worldPanel.HandleCollision();
                        }
                    }
                }
            }

            double averageDist = double.MaxValue;
            if (totalObstacles > 0)
                averageDist = totalDist / totalObstacles;

            return new SensorResult() { AverageDistance = averageDist};
        }

        public struct SensorResult
        {
            // The average distance of all obstacles in range
            public double AverageDistance { get; set; }
        }
    }
}
