﻿namespace SelfDrivingCarSimulator
{
    /**
     * Base class for sensors
     */
    public class SensorBase
    {
        protected World _world;
        private int _sensor_range = 200;
        protected WorldPanel _worldPanel;

        public int WorldWidth { get { return _worldPanel.Width; } }
        public int WorldHeight { get { return _worldPanel.Height; } }

        public SensorBase(WorldPanel worldPanel, World world)
        {
            _worldPanel = worldPanel;
            _world = world;
        }

        protected void SetRange(int range)
        {
            if (range > 0)
                _sensor_range = range;
        }

        public int GetRange()
        {
            return _sensor_range;
        }
    }
}
