﻿using System.Drawing;

namespace SelfDrivingCarSimulator
{
    /*
         * Helper class for graph plotting colors
         */
    public static class BrushPalette
    {
        public static Brush BlueBrush
        {
            get; private set;
        } = new SolidBrush(Color.MediumBlue);

        public static Brush GreenBrush
        {
            get; private set;
        } = new SolidBrush(Color.Green);

        public static Brush CanvasBrush
        {
            get; private set;
        } = new SolidBrush(Color.White);

        public static Brush BlackBrush
        {
            get; private set;
        } = new SolidBrush(Color.Black);

        public static Pen RedPen
        {
            get; private set;
        } = new Pen(Color.Red, 1);

        public static Pen BlackPen
        {
            get; private set;
        } = new Pen(Color.Black, 0.5f);

        public static Pen DarkBluePen
        {
            get; private set;
        } = new Pen(Color.DarkBlue, 1);

        public static Pen DarkGreenPen
        {
            get; private set;
        } = new Pen(Color.DarkGreen, 1);

        public static Pen DarkOrangePen
        {
            get; private set;
        } = new Pen(Color.DarkOrange, 1);

        public static Pen BlueVioletPen
        {
            get; private set;
        } = new Pen(Color.BlueViolet, 1);

        public static Pen GreenPen
        {
            get; private set;
        } = new Pen(Color.Green, 1);

        public static Pen GrayPen
        {
            get; private set;
        } = new Pen(Color.DarkSlateGray, 1);
    }
}
